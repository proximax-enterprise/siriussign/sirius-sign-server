const Stripe = require("stripe")(process.env.STRIPE_SECRET_KEY);
const SiriusChainHelper = require("../helper/sirius-chain");
const https = require('https');

var xpx2usdRate = 0;
var timestamp = 0;

/**
 * GET Stripe key to construct Stripe Object and exchange rate
 */
async function getStripeKey(req, res) {
    const rate = await fetchRate();
    const roundedRate = Math.ceil(rate * 1000) / 1000;
    res.send({
        publicKey: process.env.STRIPE_PUBLISHABLE_KEY,
        rate: roundedRate
    });
};

/**
 * Fetch XPX<->USD exchange rate from coinapi.io
 */
function fetchRate() {
    console.log("*** Current rate:");
    console.log(xpx2usdRate);
    console.log(timestamp);
    const now = Date.now();
    if (timestamp - now > 3600000 || timestamp == 0 || xpx2usdRate == 0) {
        const options = {
            "method": "GET",
            "hostname": "rest.coinapi.io",
            "path": "/v1/exchangerate/XPX/USD",
            "headers": { 'X-CoinAPI-Key': process.env.COINAPI_API_KEY }
        };
        return new Promise((resolve, reject) => {
            const request = https.request(options, function (response) {
                var chunks = [];
                response.on("data", function (chunk) {
                    chunks.push(chunk);
                    const exchangeInfo = JSON.parse(chunks.toString());
                    xpx2usdRate = exchangeInfo.rate;
                    timestamp = (new Date(exchangeInfo.time)).getTime();
                    console.log('Fetched exchange info: ');
                    console.log(exchangeInfo);
                    resolve(xpx2usdRate);
                });
            });

            request.end();
        });
    }
    else {
        return new Promise((resolve, reject) => resolve(xpx2usdRate));
    }
}

/**
 * GET exchange rate
 */
async function getRate(req, res) {
    const rate = await fetchRate(null, null);
    console.log('Exchange Rate: ', rate);
    const roundedRate = Math.ceil(rate * 1000) / 1000;
    res.send({ rate: roundedRate });
}

/**
 * POST info to confirm payment and buy crypto
 */
async function pay(req, res) {
    console.log("-----------------------------");
    const { token, currency, usdAmount, publicKey, network, xpxAmount } = req.body;
    // const info = JSON.parse(req.body);
    console.log('token    : ', token);
    console.log('usdAmount: ', usdAmount);
    console.log('publicKey: ', publicKey);
    console.log('networkType: ', network);
    console.log('xpxAmount: ', xpxAmount);

    try {
        // Create a charge with the token sent by the client
        const charge = await Stripe.charges.create({
            amount: usdAmount,
            currency: currency,
            source: token
        });

        const successTx = await SiriusChainHelper.sendXpx(publicKey, network, xpxAmount);
        console.log(successTx);
        // That's it! You're done! The payment was processed 
        console.log("[SUCCESS] Pay Done!");
        console.log("-----------------------------");
        res.send(charge);
    } catch (e) {
        // Handle "hard declines" e.g. insufficient funds, expired card, etc
        // See https://stripe.com/docs/declines/codes for more
        console.log('[ERROR] ', e.message);
        console.log(e);
        console.log("-----------------------------");
        res.send({ error: e.message });
    }
}

module.exports = {
    getStripeKey,
    pay,
    getRate
}
