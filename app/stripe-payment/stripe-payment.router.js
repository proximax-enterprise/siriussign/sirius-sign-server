const express = require('express');
const router = express.Router();
const stripePayment = require("./stripe-payment.service");

router.get("/stripe-key", (req, res) => stripePayment.getStripeKey(req, res));
router.post("/pay", (req, res) => stripePayment.pay(req, res));
router.get("/exchange", (req, res) => stripePayment.getRate(req, res));

module.exports = router;