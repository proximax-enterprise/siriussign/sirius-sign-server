const Sirius = require("tsjs-xpx-chain-sdk");
const SiriusChainConfig = require("../../config/sirius-chain.config");

/**
 * Get websocket address from api node address
 * @param {string} apiNode 
 */
function getWebsocket(apiNode) {
    const ws = apiNode.replace('https', 'wss').replace('http', 'ws');
    return ws;
}

/**
 * Check node status
 * @param {string} apiNode 
 */
function checkNode(apiNode) {
    const nodeHttp = new Sirius.NodeHttp(apiNode);
    return new Promise(resolve => {
        setTimeout(() => {
            this.isNodeWorking = false;
            resolve(false);
        }, 3000);

        nodeHttp.getNodeInfo().subscribe(nodeInfo => {
            this.isNodeWorking = true;
            resolve(true);
        });
    });
}

/**
 * Fetch generation hash
 * @param {string} apiNode 
 */
async function fetchGenerationHash(apiNode) {
    const blockHttp = new Sirius.BlockHttp(apiNode);
    const blockInfo = await blockHttp.getBlockByHeight(1).toPromise();
    const networkGenerationHash = blockInfo.generationHash;
    return networkGenerationHash;
}

/**
 * Announce and wait for transaction confirmed
 * @param {SignedTransaction} signedTx 
 * @param {string} apiNode 
 */
function announceAndWaitConfirmed(signedTx, apiNode) {
    const transactionHttp = new Sirius.TransactionHttp(apiNode);
    const ws = getWebsocket(apiNode);
    const listener = new Sirius.Listener(ws);
    const address = Sirius.Address.createFromPublicKey(signedTx.signer);
    const signedTxHash = signedTx.hash;

    return new Promise((resolve, reject) => {
        listener.open().then(() => {
            const statusSub = listener.status(address)
                .subscribe(
                    status => {
                        clearTimeout(timeoutListener);
                        reject(status);
                    },
                    error => { reject(error.status); },
                    () => { console.log('[Listener] Status Listener Done'); }
                );

            const confirmSub = listener.confirmed(address)
                .subscribe(
                    successTx => {
                        if (successTx && successTx.transactionInfo && successTx.transactionInfo.hash === signedTxHash) {
                            console.log('[Listener] Transaction is on the network now');
                            console.log(successTx);
                            clearTimeout(timeoutListener);
                            resolve(successTx);
                            confirmSub.unsubscribe();
                            statusSub.unsubscribe();
                        }
                    },
                    error => { reject(error.message); },
                    () => { console.log('[Listener] Confirmed Listener Done.'); }
                );

            transactionHttp.announce(signedTx).subscribe(res => {
                console.log("[Sirius] API Node is recieved the Signed Transaction");
                console.log("    ", signedTxHash);
            }, err => {
                console.log("[transactionHttp] Announce Error");
                console.log(err);
                reject(err.message);
            });

            const timeoutListener = setTimeout(async () => {
                const status = await transactionHttp.getTransactionStatus(signedTxHash).toPromise()
                    .catch(err => {
                        console.log("[transactionHttp] Get Tx Status Error");
                        console.log(err);
                        reject(err.message);
                    });

                if (status.group == 'unconfirmed') {
                    const statusPolling = setInterval(async () => {
                        const statusReget = await transactionHttp.getTransactionStatus(signedTxHash).toPromise();
                        if (statusReget.group == 'confirmed') {
                            const tx = await transactionHttp.getTransaction(signedTxHash).toPromise();
                            resolve(tx);
                            clearInterval(statusPolling);
                        }
                        if (statusReget.group == 'failed') {
                            reject(statusReget.status);
                            clearInterval(statusPolling);
                        }
                    }, 500);
                }

                if (status.group == 'confirmed') {
                    const tx = await transactionHttp.getTransaction(signedTxHash).toPromise();
                    resolve(tx);
                }

                if (status.group == 'failed') {
                    reject(status.status);
                }
            }, 30 * 1000);
        });
    })
}

/**
 * Send XPX to user account
 * @param {string} publicKey User Sirius public key
 * @param {number} amount XPX amount
 */
async function sendXpx(publicKey, networkType, amount) {
    const apiNode = networkType == Sirius.NetworkType.TEST_NET ? SiriusChainConfig.apiNode.testnet :
        networkType == Sirius.NetworkType.PRIVATE_TEST ? SiriusChainConfig.apiNode.privatetest : SiriusChainConfig.apiNode.testnet;
    const recipientPublicAccount = Sirius.PublicAccount.createFromPublicKey(publicKey, networkType);
    const xpxAmount = new Sirius.Mosaic(SiriusChainConfig.namespaceId, Sirius.UInt64.fromUint(amount));
    const message = Sirius.PlainMessage.create("Purchase Sirius Sign");
    const tx = Sirius.TransferTransaction.create(
        Sirius.Deadline.create(),
        recipientPublicAccount.address,
        [],
        message,
        networkType,
        Sirius.UInt64.fromUint(0)
    );
    const privateKey = networkType == Sirius.NetworkType.TEST_NET ? process.env.SIRIUS_SIGN_PRIVATE_KEY_TEST_NET :
        networkType == Sirius.NetworkType.PRIVATE_TEST ? process.env.SIRIUS_SIGN_PRIVATE_KEY_PRIVATE_TEST : process.env.SIRIUS_SIGN_PRIVATE_KEY_TEST_NET;
    const masterAccount = Sirius.Account.createFromPrivateKey(privateKey, networkType);
    const generationHash = await fetchGenerationHash(apiNode);
    const signedTx = masterAccount.sign(tx, generationHash);
    return announceAndWaitConfirmed(signedTx, apiNode);
}

module.exports = {
    getWebsocket,
    checkNode,
    fetchGenerationHash,
    announceAndWaitConfirmed,
    sendXpx
}