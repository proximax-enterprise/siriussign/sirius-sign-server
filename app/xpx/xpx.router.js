const express = require('express');
const router = express.Router();
const Xpx = require("./xpx.service");

router.post("/free", (req, res) => Xpx.sendInitXpx(req, res));

module.exports = router;