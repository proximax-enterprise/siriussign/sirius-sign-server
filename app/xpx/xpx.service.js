const SiriusChainHelper = require("../helper/sirius-chain")

async function sendInitXpx(req, res) {
    const { publicKey, network } = req.body;
    const tx = await SiriusChainHelper.sendXpx(publicKey, network, 2);
    res.send(tx);
}

module.exports = {
    sendInitXpx
}