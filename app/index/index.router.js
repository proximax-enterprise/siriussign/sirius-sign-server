const express = require('express');
const router = express.Router();

router.get("/", (req, res) => {
    const path = "./index.html"
    res.sendFile(path, {root: __dirname });
});

module.exports = router;