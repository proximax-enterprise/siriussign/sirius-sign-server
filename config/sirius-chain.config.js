const Sirius = require("tsjs-xpx-chain-sdk");

const siriusChainConfig = {
    networkType: Sirius.NetworkType.TEST_NET,
    namespaceId: new Sirius.NamespaceId('prx.xpx'),
    apiNode: {
        testnet: "https://bctestnet1.brimstone.xpxsirius.io",
        privatetest: "https://demo-sc-api-1.ssi.xpxsirius.io"
    },
    apiNodeList: {
        testnet: [
            "https://bctestnet1.brimstone.xpxsirius.io",
            "https://bctestnet2.brimstone.xpxsirius.io",
            "https://bctestnet3.brimstone.xpxsirius.io"
        ],
        privatetest: [
            "https://demo-sc-api-1.ssi.xpxsirius.io"
        ]
    }
}

module.exports = siriusChainConfig;
