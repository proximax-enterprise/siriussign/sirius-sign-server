# Sirius Sign Server
This server serves as a component in Sirius Sign payment system via Stripe that allows users to buy crypto used in this app.

## Config
### Stripe and Sirius account
Register a Stripe account at here https://dashboard.stripe.com/register  
Rename file ```env.example``` to ```.env```  
Replace existing keys with your Stripe keys and Sirius chain account key (use to sell crypto)  
Please get a free API key at ```https://www.coinapi.io/``` and replace the existing one. This is to to get exchange rate from coinapi.io.

### Sirus chain
Setup NetworkType, NamespaceId, API Node Addresses,... in file ```./config/sirius-chain.config.js```

## Install and Run
Follow these commands:  
```npm install```  
```npm start```  

## API
|         API        | Method |            Description           |
|--------------------|:------:|----------------------------------|
| /stripe/stripe-key |  GET   | Stripe public key, exchange rate |
| /stripe/pay        |  POST  | Card payment to buy crypto       |
| /stripe/exchange   |  GET   | Get exchange rate                |