const express = require("express");
const cors = require('cors');
const app = express();

const allowedOrigins = [
    'capacitor://localhost',
    'ionic://localhost',
    'http://localhost',
    'http://localhost:8080',
    'http://localhost:8100'
];

// Reflect the origin if it's in the allowed list or not defined (cURL, Postman, etc.)
const corsOptions = {
    origin: (origin, callback) => {
        if (allowedOrigins.includes(origin) || !origin) {
            callback(null, true);
        } else {
            callback(new Error('Origin not allowed by CORS'));
        }
    }
}

// Enable preflight requests for all routes
app.options('*', cors(corsOptions));

// Replace if using a different env file or config
const env = require("dotenv").config({ path: "./.env" });

app.use(
    express.json({
        // We need the raw body to verify webhook signatures.
        // Let's compute it only when hitting the Stripe webhook endpoint.
        verify: function (req, res, buf) {
            if (req.originalUrl.startsWith("/webhook")) {
                req.rawBody = buf.toString();
            }
        }
    })
);

app.use(cors(corsOptions));
app.use('/', require("./app/index/index.router"));
app.use('/stripe', require("./app/stripe-payment/stripe-payment.router"));
app.use('/xpx', require("./app/xpx/xpx.router"));

app.listen(4242, () => console.log(`Node server listening on port ${4242}!`));
